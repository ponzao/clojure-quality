(ns clojure-quality.core
  (:require [clojure.string :as s])
  (:import java.io.File)
  (:gen-class))

(defn -main
  [& args]
  (let [path (nth args 0 ".")
        files (remove #(.isDirectory %) (file-seq (File. path)))
        bad-files (filter #(.contains (slurp %) "Thread/sleep") files)]
    (doseq [file bad-files]
      (println (str "You are a bad developer. `" file "` contains a `Thread/sleep`.")))))
